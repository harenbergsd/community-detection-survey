import sys
import igraph
import subprocess


comm_filename = sys.argv[1]


# Read all communities into a list
comms = []
with open(comm_filename, "r") as commFile:
    for comm in commFile:
        comms.append([int(v) for v in comm.strip().split()])
ncomms = len(comms)


# Pairwise check over all communities to see if they overlap
edges = []
for i in range(ncomms-1):
    for j in range(i+1, ncomms):
        if len(set(comms[i]) & set(comms[j])) > 0:
            edges.append((i, j))


# Create graph where communities are nodes and edges represent overlap
comm_graph = igraph.Graph(ncomms)
comm_graph.add_edges(edges)


# Label the vertices since subgraphs relabel their ids
comm_graph.vs["id"] = [i for i in range(comm_graph.vcount())]


# Write graph into dimacs (edge) format
def write_dimacs(graph, filename):
    with open(filename, "w") as fout:
        header = "p edge %d %d\n" % (len(graph.vs), len(graph.es))
        fout.write(header)
        for edge in graph.es:
            edge = tuple(v+1 for v in edge.tuple)    # add 1 for dimacs format
            fout.write("e %d %d\n" % edge)


# Read the file output by INDDGO after running serial_wis
def read_inddgo(filename):
    iset = []
    try:
        with open(filename, "r") as fin:
            for line in fin:
                if line.startswith("#"):
                    continue
                v = int(line.strip().split()[0])
                iset.append(v)
    except IOError:
        pass
    return iset 


# Find the maximum independent set of the community graph
tmp_output = "__tmp.graph"
iset = []
components = comm_graph.clusters(mode=igraph.WEAK) 
for c in components:
    if len(c) > 2000:           # Probably too hard to run 
        continue
    
    subgraph = comm_graph.subgraph(c)
    if len(subgraph.es) > 2:    # Use INDDGO, which is fast
        # Get induced subgraph
        write_dimacs(subgraph, tmp_output)

        # Run INDDGO on subgraph
        cmd = "./INDDGO/bin/serial_wis -f "+tmp_output+" -gavril -mind"
        subprocess.check_output(cmd, shell=True)

        # Convert local indepdent set to the correct ids for full graph
        local_iset = [v-1 for v in read_inddgo(tmp_output+".WIS.sol")]
        iset += [subgraph.vs["id"][v] for v in local_iset]

        # Remove files produced by this process
        subprocess.check_output("rm -f "+tmp_output+"*", shell=True)
    else:                       # Use igraph (INDDGO doesn't produce output for |E|<2)
        local_iset = subgraph.largest_independent_vertex_sets()[0]
        iset += [subgraph.vs["id"][v] for v in local_iset]
    


# Output the new communities that should be pairwise disjoint
with open(comm_filename+"_disjoint", "w") as fout:
    for i in iset:
        comm = [str(v) for v in comms[i]]
        fout.write(" ".join(comm)+"\n")

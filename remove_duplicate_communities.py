"""
This script removes any duplicate communities from a file where each line represents a community and
vertices are separated by spaces.
"""
import sys

comm_filename = sys.argv[1]

comms = []
with open(comm_filename, "r") as comm_file:
    for line in comm_file:
        comm = tuple(sorted(set(line.strip().split())))
        if not comm in comms:
            comms.append(comm)

output_filename = comm_filename+"_unique"
with open(output_filename, "w") as fout:
    for comm in comms:
        fout.write(" ".join(comm)+"\n")

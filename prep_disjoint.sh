#!/bin/bash

# This script takes a graph file and community file as input
# and cleans the graph (removes duplicate edges, self linkes, etc.),
# removes any vertices/edges not in one of the communities,
# finds the maximum set of disjoint communties,
# induces the graph on these communities,
# and remaps the vertices in range [0, |V|-1]
#
# NOTE: This script will write over the input filenames.
# Dependencies: g++, python 2.X, igraph for python, and INDDGO.
#   *** INDDGO MUST BE BUILT IN ORDER TO RUN THIS SCRIPT ***

GFILE=$1    # graph file to clean
CFILE=$2    # corresponding community file

echo Cleaning edgelist
g++ -std=c++11 clean_edgelist.cpp -o clean_edgelist
./clean_edgelist $GFILE 1
mv $GFILE"_cleaned" $GFILE

echo Removing duplicate communities
python remove_duplicate_communities.py $CFILE
mv $CFILE"_unique" $CFILE

echo "Removing lower quartile of communities (by internal density)"
python remove_lower_quartile.py $GFILE $CFILE
mv $CFILE"_threshold" $CFILE

echo "Finding the maximum set of disjoint communitites"
python disjoint_communities.py $CFILE
mv $CFILE"_disjoint" $CFILE

echo Getting subgraph incuded by community nodes
python induced_subgraph.py $GFILE $CFILE
mv $GFILE"_induced" $GFILE

echo "Remapping vertex IDs into range [0,|V|-1]"
python map_vertices.py $GFILE $CFILE
mv $GFILE"_mapped" $GFILE
mv $CFILE"_mapped" $CFILE

Community Detection Survey
==========================
This repository contains the code used in our [community detection survey [1]](http://onlinelibrary.wiley.com/doi/10.1002/wics.1319/full) to prepare the datasets and compute various metrics. 
The graphs in the paper were from the [Stanford Large Network Dataset Collection](http://snap.stanford.edu/data/); specifically, the networks with ground-truth communities.

This repo just contains the code used to generate the overlapping communities and disjoint communities. The code for the modern community detection algorithms were provided by the original authors and the traditional disjoint algorithms can be found in igraph.

To generate the data, run the the following command:
```
$ bash ./prep_overlapping.sh <graph_file> <community_file>
```
For example,
```
$ bash ./prep_overlapping.sh com-amazon.ungraph.txt com-amazon.top5000.cmty.txt
```

Similarly, `prep_disjoint.sh` can be used if you want to generate the graphs for disjoint communities. 
**NOTE: both these scripts will cause the original input files will be overwritten, so make a copy if you don't want to lose the originals.**

As the code for generating disjoint graphs has been improved, the specific graphs used in the paper have been placed in the `disjoint_from_paper` folder. 
To match the method used to generate the graphs in the paper, remove the else clause that calls igraph's largest independent set function. 
This change may still not produce an *exact* match because there are multiple maximum indepdent sets, but it should be very close.


Dependencies
------------
Check the bash scripts, but generally the following:

* g++ with c++11 standard
* Python 2.7
* [igraph for python](http://igraph.org/python/)
* INDDGO needs to be built for generating disjoint communities (should be able to just `cd` and `make`)


References
----------
[1] Harenberg, Steve, Gonzalo Bello, L. Gjeltema, Stephen Ranshous, Jitendra Harlalka, Ramona Seay, Kanchana Padmanabhan, and Nagiza Samatova. "Community detection in large-scale networks: a survey and empirical evaluation." Wiley Interdisciplinary Reviews: Computational Statistics 6, no. 6 (2014): 426-439.
import sys

graph_filename = sys.argv[1]
comm_filename = sys.argv[2]

# Load all nodes in at least one community
cnodes = set([])
with open(comm_filename, "r") as comm_file:
    for line in comm_file:
        cnodes |= set([v for v in line.strip().split()])

# Get all edges with both nodes in a least one community
edges = []
with open(graph_filename, "r") as graph_file:
    for line in graph_file:
        u,v = line.strip().split()
        if u in cnodes and v in cnodes:
            edges.append((u,v))

# Create a new graph induced by community nodes 
new_filename = graph_filename+"_induced"
with open(new_filename, "w") as fout:
    for u,v in edges:
        fout.write(u+" "+v+"\n")

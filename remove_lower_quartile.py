"""
Removes the communities which fall in the lowest quartile of densities.
"""
import sys
import igraph
import operator


graph_filename = sys.argv[1]
comm_filename = sys.argv[2]


def create_graph(filename):
    """ Returns an undirected igraph object """
    with open(filename, "r") as graphFile:
        graph = igraph.Graph.Read(graphFile, format="edgelist")
        graph.to_undirected()
    return graph


def read_communities(filename):
    """ Returns a list of communities. Each community is a set. """
    with open(filename, 'r') as commFile:
        comms = []
        for comm in commFile:
            comms.append(set([int(v) for v in comm.strip().split()]))
    return comms


def get_densities(graph, communities):
    """Get the densities of the communities in the graph """
    densities = []
    for comm in communities:
        # Create from scratch since in general the community size will be << graph size
        subg = graph.subgraph(comm, implementation="create_from_scratch")
        densities.append((subg.density(), comm))
    return sorted(densities, key=operator.itemgetter(0))


graph = create_graph(graph_filename)
comms = read_communities(comm_filename)
densities = get_densities(graph, comms)

output_filename = comm_filename+"_threshold"
with open(output_filename, 'w') as fout:
    for d, comm in densities[len(densities)/4:]:
        fout.write(" ".join([str(v) for v in comm])+"\n")

"""
Maps the community and graph vertices into the range [0,|V|-1]
"""
import sys

graph_filename = sys.argv[1]
comm_filename = sys.argv[2]

# Get all the unique vertices
verts = set([])
edges = []
with open(graph_filename, "r") as graph_file:
    for line in graph_file:
        u,v = [int(v) for v in line.strip().split()]
        verts.add(u)
        verts.add(v)
        edges.append((u,v))


# Map the vertices to a scale of 0 to |V|-1 
vmap = {}
verts = sorted(verts)
for i,v in enumerate(verts):
    vmap[v] = str(i)


# Create a new graph file with the new vertex IDs
new_filename = graph_filename+"_mapped"
with open(new_filename, "w") as fout:
    for u,v in edges:
        fout.write(vmap[u]+" "+vmap[v]+"\n")


# Create a new community file with the new vertex IDs
new_filename = comm_filename+"_mapped"
with open(comm_filename, "r") as comm_file:
    with open(new_filename, "w") as fout:
        for line in comm_file:
            mapped_comm = [vmap[int(v)] for v in line.strip().split()]
            fout.write(" ".join(mapped_comm)+"\n")


# Output the map for the vertices
output_filename = graph_filename.rsplit(".", 1)[0]+".VMAP"
with open(output_filename, "w") as map_file:
    for v in verts:
        print >> map_file, v, "-->", vmap[v]

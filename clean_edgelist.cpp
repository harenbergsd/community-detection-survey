/*
This script:
    1) Removes comments (lines starting with #, like in the graphs from SNAP)
    2) Removes self links
    3) Removes duplicate edges
    4) Outputs in edgelist or adjacency list format

It was designed to be fairly memory efficient so that it can scale to the larger graphs in the SNAP repository.
The main strategy is to loop through the edges and as soon as you have seen all the neighbors of a vertex, 
write the edges of that vertex to disk and drop the edges from memory. 
This strategy requires several passes through the file.
*/

#include <iostream>
#include <fstream>
#include <map>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>

bool UNDIRECTED=true;   // whether to make the graph undirected (edge only listed in one direction)


/* Struct used to determine when adjacency row can be dropped from memory */
typedef struct Counter {
    unsigned int outgoing_edges = 0;
    unsigned int total_edges = 0;
    unsigned int num_visits = 0;
} Counter;


/* String to unsigned int: a wrapper around the stroul function */
unsigned int strtou(const char* str, char** endptr, int base){
    unsigned long result = strtoul(str, endptr, base);
    if (result > std::numeric_limits<unsigned int>::max())
        throw std::out_of_range("strou");
    return result;
}


/* Loops through all lines to determine the number of times a vertex is in an edge */
void get_freq(std::string input_filename, std::vector<Counter> &freq_vec)
{
    unsigned int u,v;
    std::string line;
    
    std::ifstream input_file(input_filename);
    while(std::getline(input_file, line))
    {
        if (line[0] == '#') 
            continue;
       
        char* targetptr;
        u = strtou(line.c_str(), &targetptr, 10);
        v = strtou(targetptr, NULL, 10);
        
        // disregard self links
        if (u == v)
            continue;

        // if we want an undirect graph, order the edge
        if (UNDIRECTED)
        {
            if (v < u)
            {
                unsigned int tmp = u;
                u = v;
                v = tmp;
            }
        }
        
        freq_vec[u].outgoing_edges ++;
        freq_vec[u].total_edges ++;
        freq_vec[v].total_edges ++;
    }
    input_file.close();
}


/* Simply prints the frequency table to console (useful for debugging) */
void display_freq(std::vector<Counter> &m)
{
    std::cout << "Freq table\n--------\n";
    for (unsigned i=0; i<m.size(); i++)
        std::cout   << i << " : " 
                    << m[i].outgoing_edges 
                    << ", "
                    << m[i].total_edges
                    << std::endl;
    std::cout << "\n--------" << std::endl;
}


/* Simply prints the adjacency list to console (useful for debugging) */
void display_adj(std::vector<std::vector<unsigned int> > &adj_list, std::vector<Counter> &m)
{
    for (unsigned vert=0; vert<m.size(); vert++){
        if (m[vert].total_edges>0){
            std::cout << vert << ":";
            for (unsigned int i=0; i<adj_list[vert].size(); i++){
                std::cout << " " << adj_list[vert][i];
            }
            std::cout << std::endl;
        }
    }
}


/* Writes a line of the adjacency list to file in adjacency list format */
void output_list(std::ofstream &file, unsigned int vert, std::vector<unsigned int> &row)
{
    file << vert;
    for (unsigned int i=0; i<row.size(); i++)
        file << " " << row[i];
    file << "\n";   // do not flush to disk or it will be slow
}


/* Writes a line of the adjacency list to file in edgelist format */
void output_edges(std::ofstream &file, unsigned int vert, std::vector<unsigned int> &row)
{
    for (unsigned int i=0; i<row.size(); i++)
        file << vert << " " << row[i] << "\n";     // do not flush to disk or it will be slow
}


/* Builds an adjacency list assuming directed edges */
void build_adj( std::string input_filename, 
                std::vector<std::vector<unsigned int> > &adj_list, 
                std::vector<Counter> &counter,
                bool edgelist_format)
{
    unsigned int u,v;
    std::string line;
    
    std::ofstream output_file;
    if (edgelist_format)
        output_file.open(input_filename + "_cleaned");
    else
        output_file.open(input_filename + "_adj");

    std::ifstream input_file(input_filename);
    while(std::getline(input_file, line))
    {
        if (line[0] == '#') 
            continue;
        
        char* targetptr;
        u = strtou(line.c_str(), &targetptr, 10);
        v = strtou(targetptr, NULL, 10);
        
        // disregard self links
        if (u == v)
            continue;
        // if we want an undirect graph, order the edge
        if (UNDIRECTED)
        {
            if (v < u)
            {
                unsigned int tmp = u;
                u = v;
                v = tmp;
            }
        }

        // if vertex hasn't been added yet
        if (adj_list[u].size() == 0){
            adj_list[u].reserve(counter[u].outgoing_edges);
            adj_list[u].push_back(v);
        }
        else{   
            // check if vertex already in the neighborhood (duplicate check)
            std::vector<unsigned int>::iterator lbound;
            lbound = std::lower_bound(adj_list[u].begin(), adj_list[u].end(), v);
            if (lbound==adj_list[u].end() || *lbound!=v)   
                adj_list[u].insert(lbound,v);               // insert to maunsigned intain order
        }
        counter[u].num_visits ++;
        if (counter[u].num_visits == counter[u].outgoing_edges)
        {
            if (edgelist_format)
                output_edges(output_file, u, adj_list[u]);
            else
                output_list(output_file, u, adj_list[u]);
            adj_list[u] = std::vector<unsigned int>();      // drop from memory, this list is no longer needed
        }
    }
    
    // output empty rows for verts with only incoming edges
    if (!edgelist_format)
        for (v=0; v<counter.size(); v++)
            if (counter[v].outgoing_edges==0 && counter[v].total_edges>0)
                output_list(output_file, v, adj_list[v]);
    
    // close files
    input_file.close();
    output_file.close();
}


/* Loops through the given edgelist file to get the max vertex ID */
unsigned int get_maxID(std::string filename)
{
    double t1 = (double)clock() / CLOCKS_PER_SEC;

    unsigned int u,v;
    unsigned int maxID = 0;
    std::string line;
    
    std::ifstream input_file(filename);
    while(std::getline(input_file, line))
    {
        if (line[0] == '#') 
            continue;
        
        char* targetptr;
        u = strtou(line.c_str(), &targetptr, 10);
        v = strtou(targetptr, NULL, 10);
        
        if (u > maxID)
            maxID = u;
        if (v > maxID)
            maxID = v;
    }
    return maxID;
}


int main(int argc, char* argv[])
{
    if (argc != 3) {
        std::cout << "./clean_graph <graph_file> <0|1>\n"
                  << "0 = adjacency list output format\n"
                  << "1 = edgelist output format" << std::endl;
        return -1;
    }

    std::string input_filename = argv[1];
    bool edgelist_format = atoi(argv[2]);
    
    // first pass to find the maxID
    unsigned int maxID = get_maxID(input_filename);
    
    // second pass to build a frequency table
    std::vector<Counter> freq_vec(maxID+1);
    get_freq(input_filename, freq_vec);
    
    // third pass to build adjacency list and output
    std::vector<std::vector<unsigned int> > adj_list(maxID+1);
    build_adj(input_filename, adj_list, freq_vec, edgelist_format);

    return 0; 
}
